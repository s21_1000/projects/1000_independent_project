//  array to store error messages

let errors = new Array();
errors[0] = "<span style='color:red'>Please enter your first name</span>";
errors[1] = "<span style='color:red'>Please enter your last name</span>";
errors[2] = "<span style='color:red'>I was expecting an Email!</span>";
errors[3] = "<span style='color:red'>Berevity may be much desired, but here I would prefer you are chatty...</span>";


// array to store form ids

let formID = new Array("first", "last", "email", "comment");


//passing variable 'this' from contact form onblur to 'input' variable
function validate(input) {
    let errorDivID = `no_${input.id}`; //concatonation to create error message divID i.e. 'no_first'
    let inputValue = document.getElementById(input.id).value; //getting contents of form field 
    let errorIndex = 0; //initializing errorIndex
    let invalidEmail = false; //initialzing invalidEmail value

   //assigns error indexes to formID indexes, so the right message goes with the right field
    switch (input.id) {
        case formID[0]:
            errorIndex = 0;
            break;
        case formID[1]:
            errorIndex = 1;
            break;
        case formID[2]:
            errorIndex = 2;
            break;
        case formID[3]:
            errorIndex = 3;
            break;
    }

    //statement to validate email value formatting

    if (input.id == "email") {
        let atpos = inputValue.indexOf("@");
        let dotpos = inputValue.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= inputValue.length) {
            invalidEmail = true;
        }

    }
// populates error message in errorDivID by field, if error is present

    if (inputValue == "" || invalidEmail == true) {
        document.getElementById(errorDivID).innerHTML = errors[errorIndex];
    }
    //clears error message (or empty errorDivID stays blank)
    else {
        document.getElementById(errorDivID).innerHTML = "";
    }
}

//on submit, goes through each index in both arrays to check for a final count.  Each 'true' gives ++ to final count.  8 'true'values give permission to validate

function finalValidate() {

let validationCount = 0;
for (i in formID) {
   if (document.getElementById(formID[i]).innerHTML == "") {
    validationCount++;
   }
   if (document.getElementById(formID[i]).value != "") {
    validationCount++;
   }
}

if (validationCount == 8) {
    alert("Your comments have been submitted!");
    } else {
        alert("Please provide corrected information");
    }
}